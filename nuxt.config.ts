import NuxtConfiguration from '@nuxt/config';
import pkg from './package.json';

export default {
  mode: 'universal',

  srcDir: 'src/',

  head: {
    title: 'CNPJ - ConexaLabs',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    ],
  },

  loading: { color: '#3b8070' },

  router: {
    base: process.env.NODE_ENV === 'production'
      ? '/ativ-cnpj-conexalabs/'
      : '/',
  },

  css: [
    '@fortawesome/fontawesome-free/css/all.min.css',
  ],

  plugins: [
    '@/plugins/vue-the-mask.ts',
  ],

  modules: [
    '@nuxtjs/axios',
  ],

  devModules: [
    '@nuxtjs/vuetify',
  ],

  axios: {
    baseURL: 'https://www.receitaws.com.br/v1',
  },

  vuetify: {
    defaultAssets: {
      icons: false,
    },
    icons: {
      iconfont: 'fa',
    },
  },

  build: {
    postcss: {
      plugins: {
        autoprefixer: {},
      },
    },

    extend(config, { isClient, loaders: { vue } }) {
      if (isClient) {
        // eslint-disable-next-line no-param-reassign
        vue.transformAssetUrls = {};
      }
    },
  },
} as NuxtConfiguration;
