import { Vue } from 'nuxt-property-decorator';

const { mask } = require('vue-the-mask');

Vue.directive('mask', mask);
