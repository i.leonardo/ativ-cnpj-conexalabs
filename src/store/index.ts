import { set, toggle } from '@/utils/vuex';

export const state = () => ({
  dark: false,
  drawer: false,
  layout: false,
  msg: {
    bool: false,
    color: '',
    message: '',
    timeout: 0,
    btn: true,
  },
});

export const mutations = {
  toggleDark: toggle('dark'),
  setDrawer: set('drawer'),
  setLayout: set('layout'),
  setMsg: set('msg'),
  setBoolMsg: set('msg.bool'),
};
